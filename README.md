# Project 5: Brevet time calculator with Ajax and MongoDB

Student Modifier: Bryan Carl <bcarl@uoregon.edu>

Originally written by Ram Duraijana

Simple list of controle times stored in MongoDB database.

## What is in this repository

An ACP Controle Time Calculator Using Flask and Mongo DB as a framework. 

## Functionality

You may enter as many values as you would like to calculate the ACP Controle times. Once you would like to see the results, you may hit the submit button to enter your data into the database. If you would like to see your ACP Controle times simply hit the display button and it will take you to your desired page

## Time Calculations

When a distance in kilometers is divided by a speed in kilometers per hour, the result is a time measured in hours. For example, a distance of 100 km divided by a speed of 15 km per hour results in a time of 6.666666... hours. To convert that figure into hours and minutes, subtract the whole number of hours (6) and multiply the resulting fractional part by 60. The result is 6 hours, 40 minutes, expressed here as 6H40.

The calculator converts all inputs expressed in units of miles to kilometers and rounds the result to the nearest kilometer before being used in calculations. Times are rounded to the nearest minute.

## Running it

To run it you need to have docker installed. First you will open up the files in Terminal and type 

$ docker-compose up

And that is it! From there it should start up the sequence to launching the Flask-Based Mongo ACP Time Controller Calculator.
