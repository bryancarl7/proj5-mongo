import os
import arrow
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
from acp_times import open_time, close_time

app = Flask(__name__)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb


@app.route('/')
def todo():
    return render_template('todo.html')


@app.route('/new', methods=['POST'])
def new():
    # Retrieve data from form
    distance = request.form['name']
    brevet = request.form['description']
    date = request.form['date']
    time = request.form['time']

    # I'm really hoping this is what you meant by
    if time is '' or brevet is '' or date is '' or distance is '':
        return render_template('todo.html', items="Invalid Input! Please enter all fields")

    # Do operations to clean data for storage in DB
    datetime = date+" "+time
    start_time = arrow.get(datetime, "YYYY-M-D HH:mm")
    opentime = open_time(int(distance), int(brevet), start_time)
    closetime = close_time(int(distance), int(brevet), start_time)

    item_doc = {
        'open_time': opentime,
        'close_time': closetime
    }
    db.tododb.insert_one(item_doc)

    return redirect(url_for('todo'))


@app.route('/display')
def display():
    if db.tododb.count() == 0:
        return render_template('display.html', items="No Times Were Input")
    else:
        _items = db.tododb.find()
        items = [item for item in _items]
        return render_template('display.html', items=items)


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
